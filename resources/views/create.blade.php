<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="apple-touch-icon" sizes="76x76" href={{ ("paper-bootstrap-wizard/img/apple-icon.png") }} />
	<link rel="icon" type="image/png" href={{ ("paper-bootstrap-wizard/img/favicon.png") }} />
	<title>Paper Bootstrap Wizard by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!-- CSS Files -->
    <link href={{ ("paper-bootstrap-wizard/css/bootstrap.min.css") }} rel="stylesheet" />
	<link href={{ ("paper-bootstrap-wizard/css/paper-bootstrap-wizard.css") }} rel="stylesheet" />

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href={{ ("paper-bootstrap-wizard/css/demo.css") }} rel="stylesheet" />

	<!-- Fonts and Icons -->
    {{-- <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet"> --}}

    <!-- Font Awesome -->
  	{{-- <link rel="stylesheet" href={{ asset("bower_components/font-awesome/css/font-awesome.min.css") }}> --}}
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

	<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
	<link href={{ ("paper-bootstrap-wizard/css/themify-icons.css") }} rel="stylesheet">
</head>

<body>
	<div id="app">
		<div class="image-container set-full-height" style="background-image: url({{ ('paper-bootstrap-wizard/img/paper-1.jpeg') }}">

		    <!--   Big container   -->
		    <div class="container">
		        <div class="row">
			        <div class="col-sm-8 col-sm-offset-2">

			            <!--      Wizard container        -->
			            <div class="wizard-container">

			                <div class="card wizard-card" data-color="green" id="wizardProfile">
			                    <form action="" method="">
			                <!--        You can switch " data-color="orange" "  with one of the next bright colors: "blue", "green", "orange", "red", "azure"          -->

			                    	<div class="wizard-header text-center">
			                        	<h3 class="wizard-title">Create your profile</h3>
										<p class="category">This information will let us know more about you.</p>
			                    	</div>

									<div class="wizard-navigation">
										<div class="progress-with-circle">
										     <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
										</div>
										<ul>
				                            <li>
												<a href="#about" data-toggle="tab">
													<div class="icon-circle">
														<i class="ti-user"></i>
													</div>
													About
												</a>
											</li>
				                            <li>
												<a href="#account" data-toggle="tab">
													<div class="icon-circle">
														<i class="ti-settings"></i>
													</div>
													Work
												</a>
											</li>
				                            <li>
												<a href="#address" data-toggle="tab">
													<div class="icon-circle">
														<i class="ti-map"></i>
													</div>
													Address
												</a>
											</li>
				                        </ul>
									</div>
			                        <div class="tab-content">
			                            <div class="tab-pane" id="about">
			                            	<h5 class="info-text"> Please tell us more about yourself.</h5>
			                            	<div class="row">
			                            		<teachers-list></teachers-list>
											</div>
			                            </div>
			                            <div class="tab-pane" id="account">
			                                <h5 class="info-text"> What are you doing? (checkboxes) </h5>
			                                <div class="row">
			                                	<subjects-list></subjects-list>
			                                </div>
			                            </div>
			                            <div class="tab-pane" id="address">
			                                <div class="row">
			                                </div>
			                            </div>
			                        </div>
			                        <div class="wizard-footer">
			                            <div class="pull-right">
			                                <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
			                                <input type='button' class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' />
			                            </div>

			                            <div class="pull-left">
			                                <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
			                            </div>
			                            <div class="clearfix"></div>
			                        </div>
			                    </form>
			                </div>
			            </div> <!-- wizard container -->
			        </div>
		    	</div><!-- end row -->
			</div> <!--  big container -->
		    </div>
		</div>
	</div>
</body>

	<script src="js/app.js"></script>

	<!--   Core JS Files   -->
	<script src={{ ("paper-bootstrap-wizard/js/jquery-2.2.4.min.js") }} type="text/javascript"></script>
	<script src={{ ("paper-bootstrap-wizard/js/bootstrap.min.js") }} type="text/javascript"></script>
	<script src={{ ("paper-bootstrap-wizard/js/jquery.bootstrap.wizard.js") }} type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src={{ ("paper-bootstrap-wizard/js/paper-bootstrap-wizard.js") }} type="text/javascript"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src={{ ("paper-bootstrap-wizard/js/jquery.validate.min.js") }} type="text/javascript"></script>

</html>
